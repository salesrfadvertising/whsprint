<?php
/*
-- Source Code from My Notes Code (www.mynotescode.com)
--
-- Follow Us on Social Media
-- Facebook : http://facebook.com/mynotescode
-- Twitter  : http://twitter.com/mynotescode
-- Google+  : http://plus.google.com/118319575543333993544
--
-- Terimakasih telah mengunjungi blog kami.
-- Jangan lupa untuk Like dan Share catatan-catatan yang ada di blog kami.
*/

// Load file koneksi.php
include "configuration/config_connect.php";

if(isset($_POST['import'])){ // Jika user mengklik tombol Import
	// Load librari PHPExcel nya
	require_once 'PHPExcel/PHPExcel.php';

	$inputFileType = 'Excel2007';
	$inputFileName = 'tmp/data_barang.xlsx';

	$reader = PHPExcel_IOFactory::createReader($inputFileType);
	$excel = $reader->load($inputFileName);

	$numrow = 1;
	$worksheet = $excel->getActiveSheet();
	foreach ($worksheet->getRowIterator() as $row) {
		// Cek $numrow apakah lebih dari 1
		// Artinya karena baris pertama adalah nama-nama kolom
		// Jadi dilewat saja, tidak usah diimport
		if($numrow > 1){
			// START -->
			// Skrip untuk mengambil value nya
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set

			$get = array(); // Valuenya akan di simpan kedalam array,dimulai dari index ke 0
			foreach ($cellIterator as $cell) {
				array_push($get, $cell->getValue()); // Menambahkan value ke variabel array $get
			}
			// <-- END

			// Ambil data value yang telah di ambil dan dimasukkan ke variabel $get

							$no = $get[21]; // Ambil data kode
                            $kode = sprintf("%04s", $no);
                            $sku = $get[1]; // Ambil data sku
                            $nama = $get[2]; // Ambil data nama
                            $hbeli = $get[3]; // Ambil data hargabeli
                            $hjual = $get[4]; // Ambil data hargajual
                            $keterangan = $get[5]; // Ambil data keterangan
                            $kategori = $get[6]; // Ambil data kategori  
                            $satuan = $get[7]; // Ambil data satuan
                            $terjual = $get[8]; // Ambil data terjual
                            $terbeli = $get[9]; // Ambil data terbeli
                            $sisa = $get[10]; // Ambil data sisa
                            $stokmin=$get[11];
                            $stokmax=$get[12];
                            $barcode= $get[13]; // Ambil data barcode
                            $brand = $get[14]; // Ambil data brand
                            $seri = $get[15]; // Ambil data seri
                            $lokasi = $get[16]; // Ambil data lokasi
                            $expired = $get[17]; // Ambil data expired
                            $warna = $get[18]; // Ambil data warna
                            $ukuran = $get[19]; // Ambil data ukuran
                            $avatar = $get[20]; // Ambil data avatar
                            $kodesupplier = $get[22]; // Ambil data avatar

			// Cek jika semua data tidak diisi
							if($sku == "" && $nama == "" && $satuan == "" && $hbeli == "" && $hjual == "" && $stokmin == "" && $kategori == "" && $sisa == "" && $no == "" && $avatar == "")
								continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

			// Tambahkan value yang akan di insert ke variabel $query
			// Buat query Insert
			$query = "INSERT INTO barang VALUES('".$kode."','".$sku."','".$nama."','".$hbeli."','".$hjual."','".$keterangan."','".$kategori."','".$satuan."','".$terjual."','".$terbeli."','".$sisa."','".$stokmin."','".$stokmax."','".$barcode."','".$brand."','".$seri."','".$lokasi."','".$expired."','".$warna."','".$ukuran."','".$avatar."','".$no."','".$kodesupplier."')";
			mysqli_query($conn, $query);
		}

		$numrow++; // Tambah 1 setiap kali looping
	}
}
echo "<script type='text/javascript'>  alert('BERHASIL, Data Barang berhasil di import!');</script>";
header('location: barang.php'); // Redirect ke halaman awal
?>
