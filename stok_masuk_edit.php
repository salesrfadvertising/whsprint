<?php
include "configuration/config_include.php";
awalan();
?>
            <div class="content-wrapper">
                <section class="content-header">
</section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
					  <div class="col-lg-12">
                        <!-- ./col -->

<!-- SETTING START-->

<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
include "configuration/config_chmod.php";
$halaman = "stok_masuk"; // halaman
$dataapa = "Edit Barang Masuk"; // data
$tabeldatabase = "stok_masuk"; // tabel database
$chmod = $chmenu5; // Hak akses Menu
$forward = mysqli_real_escape_string($conn, $tabeldatabase); // tabel database
$forwardpage = mysqli_real_escape_string($conn, $halaman); // halaman
$search = $_POST['search'];

 function autoNumber(){
  include "configuration/config_connect.php";
  global $forward;
  $query = "SELECT MAX(RIGHT(nota, 4)) as max_id FROM $forward ORDER BY nota";
  $result = mysqli_query($conn, $query);
  $data = mysqli_fetch_array($result);
  $id_max = $data['max_id'];
  $sort_num = (int) substr($id_max, 1, 4);
  $sort_num++;
  $new_code = sprintf("%04s", $sort_num);
  return $new_code;
 }
?>


<!-- SETTING STOP -->


<!-- BREADCRUMB -->

<ol class="breadcrumb ">
<li><a href="<?php echo $_SESSION['baseurl']; ?>">Dashboard </a></li>
<li><a href="<?php echo $halaman;?>"><?php echo $dataapa ?></a></li>
<?php

if ($search != null || $search != "") {
?>
 <li> <a href="<?php echo $halaman;?>">Data <?php echo $dataapa ?></a></li>
  <li class="active"><?php
    echo $search;
?></li>
  <?php
} else {
?>
 <li class="active">Data <?php echo $dataapa ?></li>
  <?php
}
?>
</ol>

<!-- BREADCRUMB -->

<!-- BOX INSERT BERHASIL -->

         <script>
 window.setTimeout(function() {
    $("#myAlert").fadeTo(500, 0).slideUp(1000, function(){
        $(this).remove();
    });
}, 5000);
</script>

       <!-- BOX INFORMASI -->
    <?php
if ($chmod >= 2 || $_SESSION['jabatan'] == 'admin') {
	?>


	<!-- KONTEN BODY AWAL -->
                            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Data <?php echo $dataapa;?></h3>
            </div>
                                <!-- /.box-header -->

                                <div class="box-body">
								<div class="table-responsive">
    <!----------------KONTEN------------------->
      <?php
	  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	  $nota=$nosj="";
	  $nota = $_GET["nota"];



		if(($nota != null || $nota != "") && ($chmod >= 3 || $_SESSION['jabatan'] == 'admin')){

            $sql="select * from $tabeldatabase where nota='$nota'";
            $hasil2 = mysqli_query($conn,$sql);
            while ($fill = mysqli_fetch_assoc($hasil2)){
                $nota = $fill["nota"];
                $nopr = $fill["nopr"];
                $nosj = $fill["nosj"];
                $ket = $fill["ket"];
                $supplier = $fill["supplier"];
		    }
		}
		?>
	<div id="main">
	 <div class="container-fluid">

<form class="form-horizontal" method="post" action="stok_masuk_edit" id="Myform">
    <div class="box-body">
        <div class="row">
            <div class="form-group col-md-6 col-xs-12" >
                <label for="nota" class="col-sm-3 control-label">Nota:</label>
                <div class="col-sm-9">
                    <?php  if($nota == null || $nota ==""){ ?>
                        <input type="text" class="form-control" id="nota" name="nota" value="<?php echo autoNumber(); ?>" maxlength="50" required readonly>
                    <?php }else{ ?>
                        <input type="text" class="form-control" id="nota" name="nota" value="<?php echo $nota; ?>"  maxlength="50" required readonly>
                    <?php } ?>
        		</div>
            </div>
        </div>
		<div class="row">
			<div class="form-group col-md-6 col-xs-12" >
                <label for="nopr" class="col-sm-3 control-label">No PR:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="nopr" name="nopr" value="<?php echo $nopr; ?>" placeholder="No PR" maxlength="50">
                </div>
            </div>
		</div>
        <div class="row">
            <div class="form-group col-md-6 col-xs-12" >
                <label for="nosj" class="col-sm-3 control-label">No Surat Jalan:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="nosj" name="nosj" value="<?php echo $nosj; ?>" placeholder="No Surat Jalan" maxlength="50">
                </div>
            </div>
		</div>
    <div class="row">
      <div class="form-group col-md-6 col-xs-12" >
        <label for="ket" class="col-sm-3 control-label">Keterangan:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="ket" name="ket" value="<?php echo $ket; ?>" placeholder="Keterangan" maxlength="50">
        </div>
      </div>
		</div>
    <div class="row" >
      <div class="form-group col-md-6 col-xs-12" >
        <label for="supplier" class="col-sm-3 control-label">Supplier:</label>
        <div class="col-sm-9">
          <select class="form-control select2" name="supplier">
            <?php
            $sql=mysqli_query($conn,"select * from supplier");
            while ($row=mysqli_fetch_assoc($sql)){
              if ($supplier==$row['kode'])
              echo "<option value='".$row['kode']."' selected='selected'>".$row['nohp']." | ".$row['nama']."</option>";
              else
              echo "<option value='".$row['kode']."'>".$row['nohp']." | ".$row['nama']."</option>";
            }
            ?>
          </select>
        </div>
      </div>
    </div>
  </div>
    <!-- /.box-body -->
    <div class="box-footer" >
        <button type="submit" class="btn btn-default pull-left btn-flat" name="simpan" onclick="document.getElementById('Myform').submit();" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
    </div>
    <!-- /.box-footer -->
</form>
</div>
<?php


   if($_SERVER["REQUEST_METHOD"] == "POST"){

		$nota = mysqli_real_escape_string($conn, $_POST["nota"]);
        $nopr = mysqli_real_escape_string($conn, $_POST["nopr"]);
        $nosj = mysqli_real_escape_string($conn, $_POST["nosj"]);
        $ket = mysqli_real_escape_string($conn, $_POST["ket"]);
        $supplier = mysqli_real_escape_string($conn, $_POST["supplier"]);
        $sql="select * from $tabeldatabase where nota='$nota'";
        $result=mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            if($chmod >= 3 || $_SESSION['jabatan'] == 'admin'){
                $sql1 = "update $tabeldatabase set nopr='$nopr',nosj='$nosj',ket='$ket',supplier='$supplier' where nota='$nota'";
                    $updatean = mysqli_query($conn, $sql1);
                echo "<script type='text/javascript'>  alert('Berhasil, Data telah diupdate!'); </script>";
                echo "<script type='text/javascript'>window.location = '$forwardpage';</script>";
            }else{
            echo "<script type='text/javascript'>  alert('Gagal, Data gagal diupdate!'); </script>";
            echo "<script type='text/javascript'>window.location = '$forwardpage';</script>";
            }
        }else if(( $chmod >= 2 || $_SESSION['jabatan'] == 'admin')){
            $sql2 = "insert into $tabeldatabase values( '$nota','$nopr','$nosj','')";
            if(mysqli_query($conn, $sql2)){
                echo "<script type='text/javascript'>  alert('Berhasil, Data telah disimpan!'); </script>";
                echo "<script type='text/javascript'>window.location = '$forwardpage';</script>";
                }else{
                echo "<script type='text/javascript'>  alert('Gagal, Data gagal disimpan!'); </script>";
                echo "<script type='text/javascript'>window.location = '$forwardpage';</script>";
                }
        }
    }


         ?>

<script>
function myFunction() {
    document.getElementById("Myform").submit();
}
</script>

	  <!-- KONTEN BODY AKHIR -->

                                </div>
								</div>

                                <!-- /.box-body -->
                            </div>
                        </div>

<?php
} else {
?>
   <div class="callout callout-danger">
    <h4>Info</h4>
    <b>Hanya user tertentu yang dapat mengakses halaman <?php echo $dataapa;?> ini .</b>
    </div>
    <?php
}
?>
                        <!-- ./col -->
                    </div>

                    <!-- /.row -->
                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <!-- /.Left col -->
                    </div>
                    <!-- /.row (main row) -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php  footer(); ?>
            <div class="control-sidebar-bg"></div>
        </div>
          <!-- ./wrapper -->
<script src="dist/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="libs/1.11.4-jquery-ui.min.js"></script>
        <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
        <script src="dist/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="dist/plugins/morris/morris.min.js"></script>
        <script src="dist/plugins/sparkline/jquery.sparkline.min.js"></script>
        <script src="dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="dist/plugins/knob/jquery.knob.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="dist/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="dist/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="dist/plugins/fastclick/fastclick.js"></script>
        <script src="dist/js/app.min.js"></script>
        <script src="dist/js/demo.js"></script>
		<script src="dist/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="dist/plugins/datatables/dataTables.bootstrap.min.js"></script>
		<script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script src="dist/plugins/fastclick/fastclick.js"></script>
		<script src="dist/plugins/select2/select2.full.min.js"></script>
		<script src="dist/plugins/input-mask/jquery.inputmask.js"></script>
		<script src="dist/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="dist/plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<script src="dist/plugins/timepicker/bootstrap-timepicker.min.js"></script>
		<script src="dist/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy/mm/dd"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("yyyy-mm-dd", {"placeholder": "yyyy/mm/dd"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'YYYY/MM/DD h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Hari Ini': [moment(), moment()],
            'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Akhir 7 Hari': [moment().subtract(6, 'days'), moment()],
            'Akhir 30 Hari': [moment().subtract(29, 'days'), moment()],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Akhir Bulan': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

	 $('.datepicker').datepicker({
    dateFormat: 'yyyy-mm-dd'
 });

   //Date picker 2
   $('#datepicker2').datepicker('update', new Date());

    $('#datepicker2').datepicker({
      autoclose: true
    });

	 $('.datepicker2').datepicker({
    dateFormat: 'yyyy-mm-dd'
 });


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
</body>
</html>
