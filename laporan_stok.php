<!DOCTYPE html>
<html>
  <?php
  include "configuration/config_etc.php";
  include "configuration/config_include.php";
  include "configuration/config_alltotal.php";
  etc();encryption();session();connect();head();body();timing();
  pagination();

  if (!login_check()):
  echo "<meta http-equiv='refresh' content='0; url=logout' />";
  exit(0);
  endif;
  echo "<div class='wrapper'>";
  theader();
  menu();
  ?>
  <div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-lg-12">
          <!-- SETTING START-->
          <?php
          error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
          include "configuration/config_chmod.php";
          $halaman = "laporan_stok"; // halaman
          $dataapa = "Laporan Stok"; // data
          $tabeldatabase = "barang"; // tabel database
          $chmod = $chmenu9; // Hak akses Menu
          $forward = mysqli_real_escape_string($conn, $tabeldatabase); // tabel database
          $forwardpage = mysqli_real_escape_string($conn, $halaman); // halaman
          ?>
          <!-- SETTING STOP -->
          <textarea id="printing-css" style="display:none;">.no-print{display:none}</textarea>
          <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
          <script type="text/javascript">
            function printDiv(elementId) {
              var a = document.getElementById('printing-css').value;
              var b = document.getElementById(elementId).innerHTML;
              window.frames["print_frame"].document.title = document.title;
              window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
              window.frames["print_frame"].window.focus();
              window.frames["print_frame"].window.print();
            }
          </script>
          <!-- BREADCRUMB -->
          <!-- BOX HAPUS BERHASIL -->
          <script>
            window.setTimeout(function() {
              $("#myAlert").fadeTo(500, 0).slideUp(1000, function(){
                $(this).remove();
              });
            }, 5000);
          </script>
          <!-- BOX INFORMASI -->
          <?php
            if ($chmod=='1'||$chmod=='2'||$chmod=='3'||$chmod=='4'||$chmod=='5'||$_SESSION['jabatan'] == 'admin'):
              echo "";
            else:
              echo "<div class='callout callout-danger'>";
              echo "<h4>Info</h4>";
              echo "<b>Hanya user tertentu yang dapat mengakses halaman".$dataapa."ini .</b>";
              echo "</div>";
            endif;
            if ($chmod >= 1 || $_SESSION['jabatan'] == 'admin') {
              if($bulan == null || $search == "" ):
                $sqla="SELECT no, COUNT( * ) AS totaldata FROM $forward";
              else:
                $sqla="SELECT no, COUNT( * ) AS totaldata FROM $forward WHERE tglbayar BETWEEN '" . $dr . "' AND  '" . $sam . "' or kasir like '%$search%'";
              endif;
            $hasila=mysqli_query($conn,$sqla);
            $rowa=mysqli_fetch_assoc($hasila);
            $totaldata=$rowa['totaldata'];
          ?>
          <div class="box" id="tabel1">
            <div class="box-header">
              <h3 class="box-title">Data <?php echo $dataapa ?>  <span class="no-print label label-default" id="no-print"><?php echo $totaldata; ?></span></h3>
              <?php 
              if($totalinv !='' || $totalinv !=null):
                echo "<span class='no-print pull-right' id='no-print'> Total Penjualan Retail pada <b>".$namabulan."</b> ".$tahun." sejumlah <b>Rp ".number_format($totalinv, $decimal, $a_decimal, $thousand).",-</b></span>";
              endif 
              ?>
              <!-- right column -->
              <div class="col-md-12">
              <!-- Horizontal Form -->
                <div class="box-header with-border">
                </div>
                <!-- /.box-header -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.box-header -->
          <!-- /.Paginasi -->
            <?php
            error_reporting(E_ALL ^ E_DEPRECATED);
            $sql    = "select * from barang order by nama asc";
            $result = mysqli_query($conn, $sql);
            ?>
            <div class="box-body table-responsive">
              <table class="table table-hover " id="example2" >
                <thead>
                  <tr>
                    <th>No</th>
                    <th>SKU</th>
                    <th>Nama</th>
                    <th>Masuk</th>
                    <th>Keluar</th>
                    <th>Stok Sistem</th>
                    <th>Stok Aktual</th>             
                    <?php if ($chmod >= 3 || $_SESSION['jabatan'] == 'admin') {
                      echo "";
                    }else{} ?>
                  </tr>
                </thead>
                <tbody>
                <?php
                  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                  $no=1;
                  while($fill = mysqli_fetch_array($result)) :
                ?>
                
                  <tr>
                    <td><?php echo $no;?></td>
                    <td><?php  $cba =$fill['kode'];
                    $r=mysqli_fetch_assoc(mysqli_query($conn,"SELECT sku FROM barang WHERE kode='$cba'"));
                    echo mysqli_real_escape_string($conn, $r['sku']); ?></td>
                    <td><?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?></td>
                    <td>
                      <?php  
                        $kd=$fill['kode'];  
                        $a=mysqli_fetch_assoc(mysqli_query($conn, "SELECT stok_masuk.tgl as tgl, stok_masuk_daftar.kode_barang as brg, SUM(stok_masuk_daftar.jumlah) as masuk FROM stok_masuk INNER JOIN stok_masuk_daftar ON stok_masuk_daftar.nota=stok_masuk.nota WHERE stok_masuk_daftar.kode_barang='$kd' "));
                        echo $a['masuk']+0;
                      ?>
                    </td>
                    <td>
                      <?php  
                        $kd=$fill['kode'];  
                        $b=mysqli_fetch_assoc(mysqli_query($conn, "SELECT stok_keluar.tgl as tgl, stok_keluar_daftar.kode_barang as brg, SUM(stok_keluar_daftar.jumlah) as keluar FROM stok_keluar INNER JOIN stok_keluar_daftar ON stok_keluar_daftar.nota=stok_keluar.nota WHERE stok_keluar_daftar.kode_barang='$kd'"));
                        echo $b['keluar']+0;
                      ?>
                    </td>
                    <td><?php  echo mysqli_real_escape_string($conn, $a['masuk']-$b['keluar']+0); ?></td>
                    <td><?php  echo mysqli_real_escape_string($conn, $fill['sisa']); ?></td>
                  </tr>
                  <?php
                  $no++;
                  endwhile;
                  ?>
      </tbody></table>



                    </div>
                    <!-- /.box-body -->

                </div>

  <?php } else {} ?>

<iframe src="laporan_stok_print.php" style="display:none;" name="frame"></iframe>


  <div align="right"  style="padding-right:15px"  class="no-print" id="no-print" >
  <div align="left" class="no-print" id="no-print"> <a onclick="frames['frame'].print()" class="btn btn-default btn-flat" name="cetak" value="cetak"><span class="glyphicon glyphicon-print"></span></a><?php echo " "; ?>
    <a onclick="window.location.href='configuration/config_export?forward=stok&search=<?php echo $search; ?>'" class="btn btn-default btn-flat" name="cetak" value="export excel"><span class="glyphicon glyphicon-save-file"></span></a></div> <br/>
  </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
        </div>
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php footer();?>
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script src="dist/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="1-11-4-jquery-ui.min.js"></script>
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<script src="dist/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="dist/plugins/morris/morris.min.js"></script>
<script src="dist/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="dist/plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="dist/plugins/daterangepicker/daterangepicker.js"></script>
<script src="dist/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="dist/plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>
<script src="dist/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="dist/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="dist/plugins/fastclick/fastclick.js"></script>
<script src="dist/plugins/select2/select2.full.min.js"></script>
<script src="dist/plugins/input-mask/jquery.inputmask.js"></script>
<script src="dist/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="dist/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="dist/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="dist/plugins/iCheck/icheck.min.js"></script>


<!-- ChartJS 1.0.1 -->
<script src="dist/plugins/chartjs/Chart.min.js"></script>



<script>
$(document).ready(function () {
  var table = $('#example2').DataTable({
    aLengthMenu: [
      [25, 50, 100, 200, -1],
      [25, 50, 100, 200, "All"]
    ],
    iDisplayLength: -1,
    "paging": true,
    "pageLength": 50,
    "lengthChange": true,
    "searching": true,
    "ordering": true,
    "info": true,
  });
});
$(function () {
//Initialize Select2 Elements
$(".select2").select2();

//Datemask dd/mm/yyyy
$("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy/mm/dd"});
//Datemask2 mm/dd/yyyy
$("#datemask2").inputmask("yyyy-mm-dd", {"placeholder": "yyyy/mm/dd"});
//Money Euro
$("[data-mask]").inputmask();

//Date range picker
$('#reservation').daterangepicker();
//Date range picker with time picker
$('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'YYYY/MM/DD h:mm A'});
//Date range as a button
$('#daterange-btn').daterangepicker(
{
ranges: {
'Hari Ini': [moment(), moment()],
'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
'Akhir 7 Hari': [moment().subtract(6, 'days'), moment()],
'Akhir 30 Hari': [moment().subtract(29, 'days'), moment()],
'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
'Akhir Bulan': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
},
startDate: moment().subtract(29, 'days'),
endDate: moment()
},
function (start, end) {
$('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}
);

//Date picker
$('#datepicker').datepicker({
autoclose: true
});

$('.datepicker').datepicker({
dateFormat: 'yyyy-mm-dd'
});

//Date picker 2
$('#datepicker2').datepicker('update', new Date());

$('#datepicker2').datepicker({
autoclose: true
});

$('.datepicker2').datepicker({
dateFormat: 'yyyy-mm-dd'
});


//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
checkboxClass: 'icheckbox_minimal-blue',
radioClass: 'iradio_minimal-blue'
});
//Red color scheme for iCheck
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
checkboxClass: 'icheckbox_minimal-red',
radioClass: 'iradio_minimal-red'
});
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
checkboxClass: 'icheckbox_flat-green',
radioClass: 'iradio_flat-green'
});

//Colorpicker
$(".my-colorpicker1").colorpicker();
//color picker with addon
$(".my-colorpicker2").colorpicker();

//Timepicker
$(".timepicker").timepicker({
showInputs: false
});
});
</script>
</body>
</html>
