<?php
include "configuration/config_include.php";
awalan();
?>
            <div class="content-wrapper">
                <section class="content-header">
</section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
            <div class="col-lg-12">
                        <!-- ./col -->

<!-- SETTING START-->

<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
include "configuration/config_chmod.php";
$halaman = "supplier"; // data
$dataapa = "Supplier"; // data apa
$tabeldatabase = "supplier"; // tabel database
$chmod = $chmenu2; // Hak akses Menu
$forward = mysqli_real_escape_string($conn, $tabeldatabase); // tabel database
$forwardpage = mysqli_real_escape_string($conn, $halaman); // halaman
$search = $_POST['search'];
$insert = $_POST['insert'];

 function autoNumber(){
  include "configuration/config_connect.php";
  global $forward;
  $query = "SELECT kode as max_id FROM supplier ORDER BY kode DESC LIMIT 1";
  $result = mysqli_query($conn, $query);
  $data = mysqli_fetch_array($result);
  $id_max = $data['max_id'];
  $new_code = (int)$id_max;
  $new_code++;
  return $new_code;
 }
?>


<!-- SETTING STOP -->


<!-- BREADCRUMB -->

<ol class="breadcrumb ">
<li><a href="<?php echo $_SESSION['baseurl']; ?>">Dashboard </a></li>
<li><a href="<?php echo $halaman;?>"><?php echo $dataapa ?></a></li>
<?php

if ($search != null || $search != "") {
?>
 <li> <a href="<?php echo $halaman;?>">Data <?php echo $dataapa ?></a></li>
  <li class="active"><?php
    echo $search;
?></li>
  <?php
} else {
?>
 <li class="active">Data <?php echo $dataapa ?></li>
  <?php
}
?>
</ol>

<!-- BREADCRUMB -->

<!-- BOX INSERT BERHASIL -->

         <script>
 window.setTimeout(function() {
    $("#myAlert").fadeTo(500, 0).slideUp(1000, function(){
        $(this).remove();
    });
}, 5000);
</script>

       <!-- BOX INFORMASI -->
    <?php
if ($chmod >= 2 || $_SESSION['jabatan'] == 'admin') {
  ?>


  <!-- KONTEN BODY AWAL -->
                            <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Data <?php echo $dataapa;?></h3>
            </div>
                                <!-- /.box-header -->

                                <div class="box-body">
                <div class="table-responsive">
    <!----------------KONTEN------------------->
      <?php
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

    $kode=$nama=$tgldaftar=$alamat=$nohp="";
    $no = $_GET["no"];
    $insert = '1';



    if(($no != null || $no != "") && ($chmod >= 3 || $_SESSION['jabatan'] == 'admin')){
         $sql="select * from $tabeldatabase where no='$no'";
          $hasil2 = mysqli_query($conn,$sql);
          while ($fill = mysqli_fetch_assoc($hasil2)){
            $kode = $fill["kode"];
            $nama = $fill["nama"];
            $pic = $fill["pic"];
            $pkp = $fill["pkp"];
            $kota = $fill["kota"];
            $keterangan = $fill["keterangan"];
            $tgldaftar = $fill["tgldaftar"];
            $alamat = $fill["alamat"];
            $nohp = $fill["nohp"];
            $insert = '3';
          }
    }
    ?>
  <div id="main">
   <div class="container-fluid">

          <form class="form-horizontal" method="post" action="add_<?php echo $halaman; ?>" id="Myform">
              <div class="box-body">

        <div class="row">
                <div class="form-group col-md-6 col-xs-12" >
                  <label for="kode" class="col-sm-3 control-label">Kode Supplier:</label>
                  <div class="col-sm-9">
                   <?php  if($no == null || $no ==""){ ?>
                    <input type="text" class="form-control" id="kode" name="kode" value="<?php echo autoNumber(); ?>" maxlength="50" required readonly>
                  <?php }else{ ?>
             <input type="text" class="form-control" id="kode" name="kode" value="<?php echo $kode; ?>"  maxlength="50" required readonly>
          <?php } ?>
          </div>
                </div>
        </div>

        <div class="row">
           <div class="form-group col-md-6 col-xs-12" >
                  <label for="nama" class="col-sm-3 control-label">Nama Supplier:</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $nama; ?>" placeholder="Masukan Nama Supplier" maxlength="50" required>
                  </div>
                </div>
        </div>

        <div class="row">
           <div class="form-group col-md-6 col-xs-12" >
                  <label for="tgldaftar" class="col-sm-3 control-label">Tanggal Daftar:</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control pull-right" id="datepicker2" name="tgldaftar" placeholder="Masukan Tanggal Daftar" value="<?php echo $tgldaftar; ?>" >
                  </div>
                </div>
        </div>
        <div class="row">
           <div class="form-group col-md-6 col-xs-12" >
                  <label for="pic" class="col-sm-3 control-label">Nama PIC:</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="pic" name="pic" value="<?php echo $pic; ?>" placeholder="Masukan Nama PIC" maxlength="50" required>
                  </div>
                </div>
        </div>
        <div class="row">
           <div class="form-group col-md-6 col-xs-12" >
                  <label for="pkp" class="col-sm-3 control-label">PKP:</label>
                  <div class="col-sm-9">
                    <select class="form-control select2" style="width: 100%;" name="pkp" id="pkp">
                      <?php
                        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                        if ($pkp==1){
                          echo "<option value='1' selected='selected'>PKP</option>";
                          echo "<option value='2'>Non PKP</option>";
                        }else{
                          echo "<option value='1'>PKP</option>";
                          echo "<option value='2' selected='selected'>Non PKP</option>";  
                        }
                      ?>
                    </select>
                  </div>
                </div>
        </div>

        <div class="row">
           <div class="form-group col-md-6 col-xs-12" >
                  <label for="nohp" class="col-sm-3 control-label">No Handphone:</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="nohp" name="nohp" value="<?php echo $nohp; ?>" placeholder="Masukan Nomor Handphone" maxlength="50" required>
                  </div>
                </div>
        </div>

        <div class="row">
           <div class="form-group col-md-6 col-xs-12" >
                  <label for="alamat" class="col-sm-3 control-label">Alamat:</label>
                  <div class="col-sm-9">
                  <textarea class="form-control" rows="3" id="alamat" name="alamat" maxlength="255" placeholder="Alamat Lengkap" required><?php echo $alamat; ?></textarea>
                   </div>
                </div>
        </div>
        <div class="row">
           <div class="form-group col-md-6 col-xs-12" >
                  <label for="kota" class="col-sm-3 control-label">Kota:</label>
                  <div class="col-sm-9">
                  <textarea class="form-control" rows="3" id="kota" name="kota" maxlength="255" placeholder="Kota" required><?php echo $kota; ?></textarea>
                   </div>
                </div>
        </div>
        <div class="row">
           <div class="form-group col-md-6 col-xs-12" >
                  <label for="keterangan" class="col-sm-3 control-label">Keterangan:</label>
                  <div class="col-sm-9">
                  <textarea class="form-control" rows="3" id="keterangan" name="keterangan" maxlength="255" placeholder="Keterangan" required><?php echo $keterangan; ?></textarea>
                   </div>
                </div>
        </div>


      <input type="hidden" class="form-control" id="insert" name="insert" value="<?php echo $insert;?>" maxlength="1" >


              </div>
              <!-- /.box-body -->
              <div class="box-footer" >
                <button type="submit" class="btn btn-default pull-left btn-flat" name="simpan" onclick="document.getElementById('Myform').submit();" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
              </div>
              <!-- /.box-footer -->


 </form>
</div>
<?php


   if($_SERVER["REQUEST_METHOD"] == "POST"){

          $kode = mysqli_real_escape_string($conn, $_POST["kode"]);
          $nama = mysqli_real_escape_string($conn, $_POST["nama"]);
          $pic = mysqli_real_escape_string($conn, $_POST["pic"]);
          $pkp = mysqli_real_escape_string($conn, $_POST["pkp"]);
          $tgldaftar = mysqli_real_escape_string($conn, $_POST["tgldaftar"]);
          $nohp = mysqli_real_escape_string($conn, $_POST["nohp"]);
          $alamat = mysqli_real_escape_string($conn, $_POST["alamat"]);
          $kota = mysqli_real_escape_string($conn, $_POST["kota"]);
          $keterangan = mysqli_real_escape_string($conn, $_POST["keterangan"]);
          $insert = ($_POST["insert"]);


             $sql="select * from $tabeldatabase where kode='$kode'";
        $result=mysqli_query($conn,$sql);

              if(mysqli_num_rows($result)>0){
          if($chmod >= 3 || $_SESSION['jabatan'] == 'admin'){
                  $sql1 = "update $tabeldatabase set tgldaftar='$tgldaftar',nama='$nama',pic='$pic',pkp='$pkp',nohp='$nohp',alamat='$alamat',kota='$kota',keterangan='$keterangan' where kode='$kode'";
                  $updatean = mysqli_query($conn, $sql1);
                  echo "<script type='text/javascript'>  alert('Berhasil, Data telah diupdate!'); </script>";
                  echo "<script type='text/javascript'>window.location = '$forwardpage';</script>";
        }else{
          echo "<script type='text/javascript'>  alert('Gagal, Data gagal diupdate!'); </script>";
          echo "<script type='text/javascript'>window.location = '$forwardpage';</script>";
          }
        }
      else if(( $chmod >= 2 || $_SESSION['jabatan'] == 'admin')){
           $sql2 = "insert into $tabeldatabase values( '$kode','$tgldaftar','$nama','$pic','$pkp','$alamat','$kota','$keterangan','$nohp','')";
           if(mysqli_query($conn, $sql2)){
           echo "<script type='text/javascript'>  alert('Berhasil, Data telah disimpan!'); </script>";
           echo "<script type='text/javascript'>window.location = '$forwardpage';</script>";
         }else{
           echo "<script type='text/javascript'>  alert('Gagal, Data gagal disimpan!'); </script>";
           echo "<script type='text/javascript'>window.location = '$forwardpage';</script>";
         }
           }

  }


         ?>

<script>
function myFunction() {
    document.getElementById("Myform").submit();
}
</script>

    <!-- KONTEN BODY AKHIR -->

                                </div>
                </div>

                                <!-- /.box-body -->
                            </div>
                        </div>

<?php
} else {
?>
   <div class="callout callout-danger">
    <h4>Info</h4>
    <b>Hanya user tertentu yang dapat mengakses halaman <?php echo $dataapa;?> ini .</b>
    </div>
    <?php
}
?>
                        <!-- ./col -->
                    </div>

                    <!-- /.row -->
                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <!-- /.Left col -->
                    </div>
                    <!-- /.row (main row) -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php  footer(); ?>
            <div class="control-sidebar-bg"></div>
        </div>
          <!-- ./wrapper -->
<script src="dist/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="libs/1.11.4-jquery-ui.min.js"></script>
        <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
        <script src="dist/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="dist/plugins/morris/morris.min.js"></script>
        <script src="dist/plugins/sparkline/jquery.sparkline.min.js"></script>
        <script src="dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="dist/plugins/knob/jquery.knob.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="dist/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="dist/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="dist/plugins/fastclick/fastclick.js"></script>
        <script src="dist/js/app.min.js"></script>
        <script src="dist/js/demo.js"></script>
    <script src="dist/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="dist/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="dist/plugins/fastclick/fastclick.js"></script>
    <script src="dist/plugins/select2/select2.full.min.js"></script>
    <script src="dist/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="dist/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="dist/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="dist/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="dist/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy/mm/dd"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("yyyy-mm-dd", {"placeholder": "yyyy/mm/dd"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'YYYY/MM/DD h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Hari Ini': [moment(), moment()],
            'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Akhir 7 Hari': [moment().subtract(6, 'days'), moment()],
            'Akhir 30 Hari': [moment().subtract(29, 'days'), moment()],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Akhir Bulan': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

   $('.datepicker').datepicker({
    dateFormat: 'yyyy-mm-dd'
 });

   //Date picker 2
   $('#datepicker2').datepicker('update', new Date());

    $('#datepicker2').datepicker({
      autoclose: true
    });

   $('.datepicker2').datepicker({
    dateFormat: 'yyyy-mm-dd'
 });


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
</body>
</html>
