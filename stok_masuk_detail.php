<?php
include "configuration/config_include.php";
awalan();
?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <div class="content-wrapper">
        <section class="content-header">
        </section>
        <section class="content">
            <div class="row">
              <div class="col-lg-12">
                <!-- SETTING START-->
                <?php
                  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                  include "configuration/config_chmod.php";
                  $halaman = "stok_masuk"; // halaman
                  $dataapa = "Barang Masuk"; // data
                  $tabeldatabase = "stok_masuk"; // tabel database
                  $chmod = $chmenu5; // Hak akses Menu
                  $forward = mysqli_real_escape_string($conn, $tabeldatabase); // tabel database
                  $forwardpage = mysqli_real_escape_string($conn, $halaman); // halaman
                ?>
                <!-- SETTING STOP -->
                <!-- BREADCRUMB -->
                <ol class="breadcrumb ">
                  <li><a href="<?php echo $_SESSION['baseurl']; ?>">Dashboard </a></li>
                  <li><a href="<?php echo $halaman;?>"><?php echo $dataapa ?></a></li>
                </ol>
                <!-- BREADCRUMB -->
                <!-- BOX HAPUS BERHASIL -->
                <script>
                  window.setTimeout(function() {
                      $("#myAlert").fadeTo(500, 0).slideUp(1000, function(){
                          $(this).remove();
                      });
                  }, 5000);
                </script>
                <?php
                  $hapusberhasil = $_POST['hapusberhasil'];
                  if ($hapusberhasil == 1):
                ?>
                <div id="myAlert"  class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Berhasil!</strong> <?= $dataapa;?> telah berhasil dihapus dari Data <?= $dataapa;?>.
                </div>
                <!-- BOX HAPUS BERHASIL -->
                <?php
                  elseif ($hapusberhasil == 2):
                ?>
                <div id="myAlert" class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Gagal!</strong> <?php echo $dataapa;?> tidak bisa dihapus dari Data <?php echo $dataapa;?> karena telah melakukan transaksi sebelumnya, gunakan menu update untuk merubah informasi <?php echo $dataapa;?> .
                </div>
                <?php
                  elseif ($hapusberhasil == 3):
                ?>
                <div id="myAlert" class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Gagal!</strong> Hanya user tertentu yang dapat mengupdate Data <?php echo $dataapa;?> .
                </div>
                <?php
                endif;
                if ($chmod == '1' || $chmod == '2' || $chmod == '3' || $chmod == '4' || $chmod == '5' || $_SESSION['jabatan'] == 'admin'):
                  echo "";
                else:
                ?>
                <div class="callout callout-danger">
                  <h4>Info</h4>
                  <b>Hanya user tertentu yang dapat mengakses halaman <?php echo $dataapa;?> ini .</b>
                </div>
                <?php
                endif;
                  if ($chmod >= 1 || $_SESSION['jabatan'] == 'admin'):
                    $sqla="SELECT no, COUNT( * ) AS totaldata FROM $forward";
                    $hasila=mysqli_query($conn,$sqla);
                    $rowa=mysqli_fetch_assoc($hasila);
                    $totaldata=$rowa['totaldata'];
                  endif;
                ?>
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Data Detail Barang Masuk <span class="label label-default"><?php echo $totaldata; ?></span></h3> &nbsp; &nbsp;
                  </div>
                  <div class="box-header">
                    <div class="row">
                      <form method="get" action="">
                        <div class="col col-md-4">
                          <div class="input-group input-group-sm">
                              <select class="form-control select2" name="supplier" id="supplier" style="width:150%;">
                                <option selected="selected" value="">Pilih Supplier</option>
                                  <?php
                                  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                                  $sql=mysqli_query($conn,"select *,supplier.nama as nama, supplier.kode as kode from supplier");
                                  while ($row=mysqli_fetch_assoc($sql)){
                                    echo "<option value='".$row['kode']."'>".$row['nama']."</option>";
                                  }
                                  ?>
                              </select>
                          </div>
                        </div>
                        <div class="col col-md-8 ml-auto">
                          <div class="col col-md-4 ml-auto">
                            <div class="col-md-2">
                              Awal:
                            </div>
                            <div class="col-md-4">
                              <div class="span2"><input type="text" id="datepicker" name="dari" autocomplete="off" placeholder="Dari"></div>
                            </div>
                          </div>
                          <div class="col col-md-4 ml-auto">
                            <div class="col-md-2">
                              Akhir:
                            </div>
                            <div class="col-md-4">
                              <div class="span2"><input type="text" id="datepicker2" name="sampai"></div>
                            </div>
                          </div>
                            <div class="col col-md-2 ml-auto">
                              <button type="submit" name="find" class="btn bg-maroon">Tampilkan</button>
                            </div>
                          </div>
                        </div>
                        
                      </form>
                    </div>
                  <!-- /.box-header -->
                  <!-- /.Paginasi -->
                  <?php
                  if(isset($_GET["find"])){
                    $dr = $_GET['dari'];
                    $sam = $_GET['sampai'];
                    if($_GET['supplier']!="" AND $dr!="" AND $sam!=""){
                      $dari=date("d-m-Y",strtotime($dr));
                      $sampe=date("d-m-Y",strtotime($sam));
                      $supplier = $_GET['supplier'];
                      $sql    = "SELECT a.*,b.nama as nama_barang,b.jumlah as jumlah_barang,c.brand,c.sku,c.satuan,d.nama as nama_supplier FROM  $forward a left join stok_masuk_daftar b on a.nota=b.nota left join barang c on b.kode_barang=c.kode left join supplier d on a.supplier=d.kode where supplier LIKE '$supplier' AND a.tgl BETWEEN '$dr' AND '$sam' order by b.nama asc";
                      $result = mysqli_query($conn, $sql);
                    }else{
                      $dari=date("d-m-Y",strtotime($dr));
                      $sampe=date("d-m-Y",strtotime($sam));
                      $sql    = "SELECT a.*,b.nama as nama_barang,b.jumlah as jumlah_barang,c.brand,c.sku,c.satuan,d.nama as nama_supplier FROM  $forward a left join stok_masuk_daftar b on a.nota=b.nota left join barang c on b.kode_barang=c.kode left join supplier d on a.supplier=d.kode WHERE a.tgl BETWEEN '$dr' AND '$sam' order by b.nama asc";
                      $result = mysqli_query($conn, $sql);
                    }
                  }else{
                    $sql    = "SELECT a.*,b.nama as nama_barang,b.jumlah as jumlah_barang,c.brand,c.sku,c.satuan,d.nama as nama_supplier FROM  $forward a left join stok_masuk_daftar b on a.nota=b.nota left join barang c on b.kode_barang=c.kode left join supplier d on a.supplier=d.kode order by b.nama asc";
                    $result = mysqli_query($conn, $sql);
                  }
                    error_reporting(E_ALL ^ E_DEPRECATED);
                  ?>
              <div class="box-body">
                <table class="table table-bordered table-hover" id="example2" style="overflow:auto" cellspacing="0">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Tanggal</th>
                          <th>No Transaksi</th>
                          <th>No PR</th>
                          <th>No Surat Jalan</th>
                          <th>Nama Produk</th>
                          <th>Merek</th>
                          <th>Seri Barang</th>
                          <th>Satuan</th>
                          <th>Qty</th>
                          <th>Supplier</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php
                    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                      $no = 1;
                      while($fill = mysqli_fetch_array($result)):
                        echo "<tr>";
                        echo "<td>".$no."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['tgl'])."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['nota'])."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['nopr'])."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['nosj'])."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['nama_barang'])."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['brand'])."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['sku'])."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['satuan'])."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['jumlah_barang'])."</td>";
                        echo "<td>".mysqli_real_escape_string($conn, $fill['nama_supplier'])."</td>";
                        echo "</tr>";
                        $no++;
                      endwhile;
                  ?>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
                    <!-- /.row -->
                    <!-- Main row -->
                    <div class="row">
                    </div>
                    <!-- /.row (main row) -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
           <?php footer();?>
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <script src="dist/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="dist/plugins/sparkline/jquery.sparkline.min.js"></script>
        <script src="dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="dist/plugins/knob/jquery.knob.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="dist/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="dist/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="dist/plugins/fastclick/fastclick.js"></script> 
        <script src="dist/plugins/select2/select2.full.min.js"></script>
        <script src="dist/js/app.min.js"></script>
        <script src="dist/js/demo.js"></script>
        <script src="dist/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="dist/plugins/fastclick/fastclick.js"></script>
        <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>
        <script>
          var date = new Date();
          //Initialize Select2 Elements
          $(".select2").select2();
          //Date picker
                $('#datepicker').datepicker('update', new Date(date.getFullYear(), date.getMonth(), 1));

                $('#datepicker').datepicker({
                  autoclose: true
                });
                
                $('.datepicker').datepicker({
                  dateFormat: 'yyyy-mm-dd'
                });
                
                //Date picker 2
                $('#datepicker2').datepicker('update', new Date());
                
                $('#datepicker2').datepicker({
                  autoclose: true
                });
                
                $('.datepicker2').datepicker({
                  dateFormat: 'yyyy-mm-dd'
                });
        </script>
        <script>
          $(document).ready(function () {
            var table = $('#example2').DataTable({
              dom: 'Bfrtip',
              buttons: [
                  'excel', 'print'
              ],
              aLengthMenu: [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
              ],
              iDisplayLength: -1,
              "paging": true,
              "pageLength": 50,
              "lengthChange": true,
              "searching": true,
              "ordering": true,
              "info": true,
            });
          });
        </script>
        
    </body>
</html>
