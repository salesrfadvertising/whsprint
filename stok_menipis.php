<?php
include "configuration/config_include.php";
awalan();
?>
    <div class="content-wrapper">
        <section class="content-header">
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-lg-12">
                <!-- ./col -->
                <!-- SETTING START-->
                <?php
                  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                  include "configuration/config_chmod.php";
                  $halaman = "stok_menipis"; // halaman
                  $dataapa = "Stok Menipis"; // data
                  $tabeldatabase = "barang"; // tabel database
                  $chmod = $chmenu8; // Hak akses Menu
                  $forward = mysqli_real_escape_string($conn, $tabeldatabase); // tabel database
                  $forwardpage = mysqli_real_escape_string($conn, $halaman); // halaman
                  $search = $_POST['search'];
                  $insert = $_POST['insert'];
                //SETTING STOP
                  $sql= "SELECT batas from backset";
                  $hasilx2=mysqli_query($conn,$sql);
                  $row=mysqli_fetch_assoc($hasilx2);
                  $alert = $row['batas'];
                  //BREADCRUMB
                ?>
                <ol class="breadcrumb ">
                  <li><a href="<?php echo $_SESSION['baseurl']; ?>">Dashboard </a></li>
                  <li><a href="<?php echo $halaman;?>"><?php echo $dataapa ?></a></li>
                  <?php
                    if ($search != null || $search != ""):
                      echo "<li><a href='".$halaman."'>Data ".$dataapa."</a></li>";
                      echo "<li class='active'>";
                      echo $search;
                      echo "</li>";
                    else:
                      echo "<li class='active'>";
                      echo "Data ".$dataapa;
                      echo "</li>";
                    endif;
                  ?>
                </ol>
                <!-- BREADCRUMB -->
                <!-- BOX INSERT BERHASIL -->
          <script>
            window.setTimeout(function() {
                $("#myAlert").fadeTo(500, 0).slideUp(1000, function(){
                    $(this).remove();
                });
            }, 5000);
          </script>


       <!-- BOX INFORMASI -->
<?php
  if ($chmod >= 2 || $_SESSION['jabatan'] == 'admin'):
?>

<div class="row">
  <div class="col-md-12">
      <?php
        error_reporting(E_ALL ^ E_DEPRECATED);
        $sql    = "select * from barang order by kode";
        $result = mysqli_query($conn, $sql);
      ?>
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Barang dengan <?php echo $dataapa;?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row mb-2">
                  <div class="status-filter col-md-5">
                    <select id="statusFilter" class="form-control" style="margin-left:10px;" >
                      <option value="">Show All</option>
                      <option value="Tidak perlu restock">Tidak perlu restock</option>
                      <option value="Segera Restock">Segera Restock</option>
                      <option value="Warning">Warning</option>
                      <option value="Overstock">Overstock</option>
                      <option value="Stock Aman">Stock Aman</option>
                    </select>
                  </div>
              </div>
              <table class="table table-bordered table-hover" id="filterTable">
                <thead>
                  <tr>
                    <th style="width: 10px" scope="col">#</th>
                    <th scope="col">Kode</th>
                    <th scope="col">Barang</th>
                    <th style="width: 40px" scope="col">Stok MIN</th>
                    <th style="width: 40px" scope="col">Stok MAX</th>
                    <th style="width: 40px" scope="col">Stok</th>
                    <th style="width: 40px" scope="col">Status</th>
                    <?php if ($chmod >= 3 || $_SESSION['jabatan'] == 'admin'): ?>
                    <th scope="col">Opsi</th>
                    <?php else: echo "";endif; ?>
                  </tr>
                </thead>
        <?php
          error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
          $search = $_POST['search'];
          $no=1;
          echo "<tbody>";
          while($fill = mysqli_fetch_array($result)):
          ?>
                <tr>
                <td scope="col"><?php echo $no;?></td>
                <td scope="col"><?php  echo mysqli_real_escape_string($conn, $fill['kode']); ?></td>
                <td scope="col"><?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?></td>
                <td scope="col"><?php  echo mysqli_real_escape_string($conn, $fill['stokmin']); ?></td>
                <td scope="col"><?php  echo mysqli_real_escape_string($conn, $fill['stokmax']); ?></td>
                <td scope="col"> <?php if($fill['sisa']>=10){?><span class="badge bg-yellow"><?php echo $fill['sisa'];?></span><?php } else { ?> <span class="badge bg-red"><?php echo $fill['sisa'];?></span> <?php  } ?></td>
                <td scope="col"> 
                  <?php if(($fill['stokmin']==0) OR ($fill['stokmin']==null)): ?>
                    <span class="badge bg-green">Tidak perlu restock</span>
                  <?php elseif($fill['sisa']==$fill['stokmin']):?>
                    <span class="badge bg-blue">Segera Restock</span>
                  <?php elseif($fill['sisa']<$fill['stokmin']):?>
                    <span class="badge bg-yellow">Warning</span>
                  <?php elseif($fill['sisa']>$fill['stokmax']): ?>
                    <span class="badge bg-red">Overstock</span>
                  <?php elseif(($fill['sisa']>$fill['stokmin']) OR ($fill['sisa']<$fill['stokmax'])): ?>
                    <span class="badge bg-green">Stock Aman</span>
                  <?php endif; ?>
                </td>
                <td scope="col"><?php  if ($chmod >= 1 || $_SESSION['jabatan'] == 'admin') : ?>
                <button type="button" class="btn btn-info btn-xs" onclick="window.location.href='stok_in?barcode=<?php echo $fill['barcode']?>'">Tambah</button>
                <?php else: echo ""; endif;?>
                </td>
              </tr>
            
          <?php
            $no++;
            endwhile;
          ?>
          </tbody>
        </table>
      </div>
      
    </div>
          <!-- /.box -->
</div> <!-- /.row -->
<?php
else:
?>
   <div class="callout callout-danger">
    <h4>Info</h4>
    <b>Hanya user tertentu yang dapat mengakses halaman <?php echo $dataapa;?> ini .</b>
    </div>
    <?php
      endif;
    ?>
                        <!-- ./col -->
                    </div>

                    <!-- /.row -->
                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <!-- /.Left col -->
                    </div>
                    <!-- /.row (main row) -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php  footer(); ?>
            <div class="control-sidebar-bg"></div>
        </div>
          <!-- ./wrapper -->

<!-- Script -->
    <script src='jquery-3.1.1.min.js' type='text/javascript'></script>

    <!-- jQuery UI -->
    <link href='jquery-ui.min.css' rel='stylesheet' type='text/css'>
    <script src='jquery-ui.min.js' type='text/javascript'></script>

<script src="dist/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="libs/1.11.4-jquery-ui.min.js"></script>

        <script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
        <script src="dist/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="dist/plugins/morris/morris.min.js"></script>
        <script src="dist/plugins/sparkline/jquery.sparkline.min.js"></script>
        <script src="dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="dist/plugins/knob/jquery.knob.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="dist/plugins/daterangepicker/daterangepicker.js"></script>
        <script src="dist/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="dist/plugins/fastclick/fastclick.js"></script>
        <script src="dist/js/app.min.js"></script>
        <script src="dist/js/demo.js"></script>
    <!-- <script src="dist/plugins/datatables/jquery.dataTables.min.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="dist/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="dist/plugins/fastclick/fastclick.js"></script>
    <script src="dist/plugins/select2/select2.full.min.js"></script>
    <script src="dist/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="dist/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="dist/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script src="dist/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="dist/plugins/iCheck/icheck.min.js"></script>

<!--fungsi AUTO Complete-->
<!-- Script -->
<script type='text/javascript' >
    $("document").ready(function () {
      $("#filterTable").dataTable({
        aLengthMenu: [
          [25, 50, 100, 200, -1],
          [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1,
        "paging": true,
        "pageLength": 50,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
      });
      //Get a reference to the new datatable
      var table = $('#filterTable').DataTable();
      //Take the Status filter drop down and append it to the datatables_filter div. 
      //You can use this same idea to move the filter anywhere withing the datatable that you want.
      $("#filterTable_filter.dataTables_filter").append($("#statusFilter"));
      
      //Get the column index for the Status column to be used in the method below ($.fn.dataTable.ext.search.push)
      //This tells datatables what column to filter on when a user selects a value from the dropdown.
      //It's important that the text used here (Status) is the same for used in the header of the column to filter
      var statusIndex = 0;
      $("#filterTable th").each(function (i) {
        if ($($(this)).html() == "Status") {
          statusIndex = i; return false;
        }
      });
      //Use the built in datatables API to filter the existing rows by the Status column
      $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
          var selectedItem = $('#statusFilter').val()
          var status = data[statusIndex];
          if (selectedItem === "" || status.includes(selectedItem)) {
            return true;
          }
          return false;
        }
      );
      //Set the change event for the Status Filter dropdown to redraw the datatable each time
      //a user selects a new filter.
      $("#statusFilter").change(function (e) {
        table.draw();
      });
      table.draw();
    });
</script>
    <script type='text/javascript' >
    $( function() {
  
        $( "#barcode" ).autocomplete({
            source: function( request, response ) {
                
                $.ajax({
                    url: "server.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: request.term
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function (event, ui) {
                $('#nama').val(ui.item.label);
                $('#barcode').val(ui.item.value); // display the selected text
                $('#hargajual').val(ui.item.hjual);
                $('#stok').val(ui.item.sisa); // display the selected text
                $('#hargabeli').val(ui.item.hbeli);
                $('#jumlah').val(ui.item.jumlah);
                $('#kode').val(ui.item.kode); // save selected id to input
                return false;
                
            }
        });

        // Multiple select
        $( "#multi_autocomplete" ).autocomplete({
            source: function( request, response ) {
                
                var searchText = extractLast(request.term);
                $.ajax({
                    url: "server.php",
                    type: 'post',
                    dataType: "json",
                    data: {
                        search: searchText
                    },
                    success: function( data ) {
                        response( data );
                    }
                });
            },
            select: function( event, ui ) {
                var terms = split( $('#multi_autocomplete').val() );
                
                terms.pop();
                
                terms.push( ui.item.label );
                
                terms.push( "" );
                $('#multi_autocomplete').val(terms.join( ", " ));

                // Id
                var terms = split( $('#selectuser_ids').val() );
                
                terms.pop();
                
                terms.push( ui.item.value );
                
                terms.push( "" );
                $('#selectuser_ids').val(terms.join( ", " ));

                return false;
            }
           
        });
    });

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }

    </script>

<!--AUTO Complete-->

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy/mm/dd"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("yyyy-mm-dd", {"placeholder": "yyyy/mm/dd"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'YYYY/MM/DD h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Hari Ini': [moment(), moment()],
            'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Akhir 7 Hari': [moment().subtract(6, 'days'), moment()],
            'Akhir 30 Hari': [moment().subtract(29, 'days'), moment()],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Akhir Bulan': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

   $('.datepicker').datepicker({
    dateFormat: 'yyyy-mm-dd'
 });

   //Date picker 2
   $('#datepicker2').datepicker('update', new Date());

    $('#datepicker2').datepicker({
      autoclose: true
    });

   $('.datepicker2').datepicker({
    dateFormat: 'yyyy-mm-dd'
 });


    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
</body>
</html>
