<?php include 'config_connect.php';
$search = $_GET['search'];
$forward = $_GET['forward'];
header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment; filename=$forward.xlsx");

?>
<?php if($forward == 'stokall'){ ?>
  <table class="table">
  <thead>
  <tr>
    <td colspan="13" style="text-align: center; ">BERITA ACARA STOCK OPNAME</td>
  </tr>
  <tr>
    <td colspan="13" style="text-align: center; ">CV IMARAH PROMOSINDO</td>
  </tr>
  <tr>
    <td colspan="13" style="text-align: center; ">PERIODE</td>
  </tr>
  <tr>
    <td colspan="13" style="text-align: center; ">Pada hari ini tanggal <?= date(); ?> telah dilakukan kegiatan stock opname secara keseluruhan, menghitung fisik yang ada di gudang logistik imarah printing berupa persediaan bahan atau material.</td>
  </tr>
  <tr>
    <td colspan="13" style="text-align: center; ">Adapun tujuan dilakukannya kegiatan stock opname yaitu agar tidak ada selisih antara jumlah bahan yang sudah dibeli oleh Procurement dan Stok fisik Barang yang ada di Gudang. Adapun hasil dari stok opname adalah sebagai berikut:</td>
  </tr>
  <tr>
    <th>No</th>
    <th>SKU</th>
    <th>Kategori</th>
    <th>Nama Item</th>
    <th>Merek</th>
    <th>Seri Barang</th>
    <th>Ukuran</th>
    <th>Stok Awal</th>
    <th>Stok Masuk</th>
    <th>Stok Keluar</th>
    <th>Stok Akhir</th>
    <th>Satuan</th>
    <th>Remark</th>
  </tr>
  </thead>
  <?php
  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
  $query1="SELECT * FROM barang order by no desc";
  $hasil = mysqli_query($conn,$query1);
  $no = 1;
  while ($fill = mysqli_fetch_assoc($hasil)){
    ?>
    <tbody>
    <tr>
    <td><?php echo ++$no_urut;?></td>
    <td>
    <?php  $cba =$fill['kode'];
    $r=mysqli_fetch_assoc(mysqli_query($conn,"SELECT sku FROM barang WHERE kode='$cba'"));
    echo mysqli_real_escape_string($conn, $r['sku']); 
    ?>
    </td>
    <td><?php  echo mysqli_real_escape_string($conn, $fill['kategori']); ?></td>
    <td>
    <?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?>
    </td>
    <td><?php  echo mysqli_real_escape_string($conn, $fill['brand']); ?></td>
    <td><?php  echo mysqli_real_escape_string($conn, $fill['lokasi']); ?></td>
    <td><?php  echo mysqli_real_escape_string($conn, $fill['keterangan']); ?></td>
    <td>Stok Awal/0</td>
    <td>
    <?php  
    $kd=$fill['kode'];  
    $a=mysqli_fetch_assoc(mysqli_query($conn, "SELECT stok_masuk.tgl as tgl, stok_masuk_daftar.kode_barang as brg, SUM(stok_masuk_daftar.jumlah) as masuk FROM stok_masuk INNER JOIN stok_masuk_daftar ON stok_masuk_daftar.nota=stok_masuk.nota WHERE stok_masuk_daftar.kode_barang='$kd' "));
    echo $a['masuk']+0;
    ?>
    </td>
    <td>
    <?php  
    $kd=$fill['kode'];  
    $b=mysqli_fetch_assoc(mysqli_query($conn, "SELECT stok_keluar.tgl as tgl, stok_keluar_daftar.kode_barang as brg, SUM(stok_keluar_daftar.jumlah) as keluar FROM stok_keluar INNER JOIN stok_keluar_daftar ON stok_keluar_daftar.nota=stok_keluar.nota WHERE stok_keluar_daftar.kode_barang='$kd'"));
    echo $b['keluar']+0;
    ?>
    </td>
    <td><?php  echo mysqli_real_escape_string($conn, $a['masuk']-$b['keluar']); ?></td>
    <!-- <td><?php // echo mysqli_real_escape_string($conn, $fill['sisa']); ?></td> -->
    <td><?php  echo mysqli_real_escape_string($conn, $fill['satuan']); ?></td>
    <td>-</td>
    </tr><?php
    ;
  }
  
  ?>
  </tbody></table>
  <?php } ?>
  
  
  <?php if($forward == 'barang'){ ?>
    <table class="table">
    <thead>
    <tr>
    <th>no</th>
    <th>ID</th>
    <th>Sku</th>
    <th>Nama</th>
    <th>Harga Beli</th>
    <th>Harga Jual</th>
    <th>Keterangan</th>
    <th>Kategori</th>
    <th>Satuan</th>
    <th>Stok keluar</th>
    <th>Stok masuk</th>
    <th>Stok tersedia</th>
    <th>Stok minimal</th>
    <th>Stok maximal</th>
    <th>Barcode</th>
    <th>Merek</th>
    <th>Seri</th>
    <th>Lokasi</th>
    <th>Kadaluwarsa</th>
    <th>Warna</th>
    <th>ukuran</th>
    <th>Avatar</th>
    <th>Kode Supplier</th>
    
    </tr>
    </thead>
    <?php
    error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    
    $query1="select * from $forward order by no";
    $hasil = mysqli_query($conn,$query1);
    $no = 1;
    while ($fill = mysqli_fetch_assoc( $hasil)){
      ?>
      <tbody>
      <tr>
      <td><?php echo mysqli_real_escape_string($conn, $fill['no']);?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['kode']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['sku']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['hbeli']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['hjual']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['keterangan']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['kategori']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['satuan']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['terjual']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['terbeli']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['sisa']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['stokmin']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['stokmax']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['barcode']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['brand']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['seri']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['lokasi']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['expired']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['warna']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['ukuran']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['avatar']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['kodesupplier']); ?></td>      
      </tr><?php
      ;
    }
    
    ?>
    </tbody></table>
    <?php } ?>
    
    
    <?php if($forward == 'mutasi'){ ?>
      <table class="table">
      <thead>
      <tr>
      <th>No</th>
      <th>Nama User</th>
      <th>Tanggal</th>
      <th>Barang</th>
      <th>Kegiatan</th>
      <th>Jumlah</th>
      <th>Stok Akhir</th>
      <th>Status</th>
      <th>Keterangan</th>
      </tr>
      </thead>
      <?php
      error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
      $query1    = "select mutasi.namauser,mutasi.tgl,mutasi.kodebarang,mutasi.status,mutasi.jumlah,mutasi.sisa,mutasi.kegiatan,mutasi.keterangan,barang.nama from mutasi inner join barang on mutasi.kodebarang=barang.kode order by mutasi.no desc";
      $hasil = mysqli_query($conn,$query1);
      $no = 1;
      while ($fill = mysqli_fetch_assoc($hasil)){
        ?>
        <tbody>
        <tr>
        <td><?php echo ++$no_urut;?></td>
        <td><?php  echo mysqli_real_escape_string($conn, $fill['namauser']); ?></td>
        <td><?php  echo mysqli_real_escape_string($conn, $fill['tgl']); ?></td>
        <td><?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?></td>
        <td><?php  echo mysqli_real_escape_string($conn, $fill['kegiatan']); ?></td>
        <td><?php  echo mysqli_real_escape_string($conn, $fill['jumlah']); ?></td>
        <td><?php  echo mysqli_real_escape_string($conn, $fill['sisa']); ?></td>
        <td><?php  echo mysqli_real_escape_string($conn, $fill['status']); ?></td>
        <td><?php  echo mysqli_real_escape_string($conn, $fill['keterangan']); ?></td>
        </tr><?php
        ;
      }
      
      ?>
      </tbody></table>
      <?php } ?>
      
      <?php if($forward == 'stok'){ ?>
      <table class="table">
      <thead>
      <tr>
      <th>No</th>
      <th>SKU</th>
      <th>Nama</th>
      <th>Masuk</th>
      <th>Keluar</th>
      <th>Stok Sistem</th>
      <th>Stok Aktual</th>   
      </tr>
      </thead>
      <?php
      error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
      $query1    = "select * from barang order by nama asc";
      $hasil = mysqli_query($conn,$query1);
      $no = 1;
      while ($fill = mysqli_fetch_assoc($hasil)){
        ?>
        <tbody>
        <tr>
          <td><?php echo ++$no_urut;?></td>
          <td><?php  $cba =$fill['kode'];
          $r=mysqli_fetch_assoc(mysqli_query($conn,"SELECT sku FROM barang WHERE kode='$cba'"));
          echo mysqli_real_escape_string($conn, $r['sku']); ?></td>
          <td><?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?></td>
          <td>
            <?php  
              $kd=$fill['kode'];  
              $a=mysqli_fetch_assoc(mysqli_query($conn, "SELECT stok_masuk.tgl as tgl, stok_masuk_daftar.kode_barang as brg, SUM(stok_masuk_daftar.jumlah) as masuk FROM stok_masuk INNER JOIN stok_masuk_daftar ON stok_masuk_daftar.nota=stok_masuk.nota WHERE stok_masuk_daftar.kode_barang='$kd' "));
              echo $a['masuk']+0;
            ?>
          </td>
          <td>
            <?php  
              $kd=$fill['kode'];  
              $b=mysqli_fetch_assoc(mysqli_query($conn, "SELECT stok_keluar.tgl as tgl, stok_keluar_daftar.kode_barang as brg, SUM(stok_keluar_daftar.jumlah) as keluar FROM stok_keluar INNER JOIN stok_keluar_daftar ON stok_keluar_daftar.nota=stok_keluar.nota WHERE stok_keluar_daftar.kode_barang='$kd'"));
              echo $b['keluar']+0;
            ?>
          </td>
          <td><?php  echo mysqli_real_escape_string($conn, $a['masuk']-$b['keluar']+0); ?></td>
          <td><?php  echo mysqli_real_escape_string($conn, $fill['sisa']); ?></td>
        </tr><?php
        ;
      }
      
      ?>
      </tbody></table>
      <?php } ?>
      
      <?php if($forward == 'perubahanstokmasuk'){
        
        $forward = 'barang';
        $dr = $_GET['dari'];
        $sam = $_GET['sampai'];
        
        if($dr!=''){
          $dari=date("d-m-Y",strtotime($dr));
        } else {
          $dari="Awal";
        }
        
        
        $sampe=date("d-m-Y",strtotime($sam));
        
        ?>
        
        <table class="table">
        <thead>
        <tr>
          <th>Dari Tanggal :</th>
          <th><?= $dr;?></th>
          <th>Sampai Tanggal :</th>
          <th><?= $sam;?></th>
        </tr>
        <tr>
        <th>No</th>
        <th>Tanggal Masuk</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Masuk</th>
        </tr>
        </thead>
        <tbody>
        <?php
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
        
        $query1="SELECT a.tgl as tgl, b.kode_barang as brg, b.jumlah as masuk, c.nama as nama_barang FROM stok_masuk a INNER JOIN stok_masuk_daftar b ON b.nota=a.nota INNER JOIN barang c ON b.kode_barang=c.kode WHERE a.tgl BETWEEN '" . $dr . "' AND  '" . $sam . "' ORDER BY b.kode_barang";
        $hasil = mysqli_query($conn,$query1);
        $no = 1;
        while ($fill = mysqli_fetch_assoc($hasil)):
          if(($fill['masuk']+0)>0):
          ?>
          <tr>
            <td><?php echo $no++;?></td>
            <td><?php  echo $fill['tgl']; ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['brg']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['nama_barang']); ?></td>
            <td>
              <?php  
                echo $fill['masuk']+0;
              ?>
            </td>
          </tr>
          <?php endif;?>
          <?php endwhile; ?>
        </tbody>
      </table>
    <?php } ?>

    <?php if($forward == 'perubahanstokkeluar'){
        
        $forward = 'barang';
        $dr = $_GET['dari'];
        $sam = $_GET['sampai'];
        
        if($dr!=''){
          $dari=date("d-m-Y",strtotime($dr));
        } else {
          $dari="Awal";
        }
        
        
        $sampe=date("d-m-Y",strtotime($sam));
        
        ?>
        
        <table class="table">
        <thead>
        <tr>
          <th>Dari Tanggal :</th>
          <th><?= $dr;?></th>
          <th>Sampai Tanggal :</th>
          <th><?= $sam;?></th>
        </tr>
        <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Keluar</th>
        
        </tr>
        </thead>
        <tbody>
        <?php
        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
        
        $query1="SELECT a.tgl as tgl, b.kode_barang as brg, b.jumlah as keluar,c.nama as nama_barang FROM stok_keluar a INNER JOIN stok_keluar_daftar b ON b.nota=a.nota INNER JOIN barang c ON b.kode_barang=c.kode WHERE a.tgl BETWEEN '" . $dr . "' AND  '" . $sam . "' ORDER BY b.kode_barang";
        
        $hasil = mysqli_query($conn,$query1);
        $no = 1;
        while ($fill = mysqli_fetch_assoc($hasil)):
          if(($fill['keluar']+0)>0):
          ?>
          <tr>
            <td><?php echo $no++;?></td>
            <td><?php  echo $fill['tgl']; ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['brg']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['nama_barang']); ?></td>
            <td>
              <?php 
                echo $fill['keluar']+0;
              ?>
            </td>
          </tr>
          <?php endif;?>
          <?php endwhile; ?>
        </tbody>
      </table>
    <?php } ?> 

    <?php if($forward == 'income'){
          
          $forward = 'bayar';
          $bulan = $_GET['bulan'];
          $tahun = $_GET['tahun'];
          
        ?>
          
          <table class="table">
          <thead>
          <tr>
          <th>No</th>
          <th>No Nota</th>
          <th>Tanggal</th>
          <th>Jumlah Item</th>
          <th>Total Masuk</th>
          <th>Total Keluar</th>
          <th>Income</th>
          <th>Cc</th>
          </tr>
          </thead>
        <?php
          error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
          if($tahun == null || $tahun == ""){
            $query1="SELECT * FROM  $forward where nota IN (SELECT nota FROM transaksimasuk) order by no ";
          }else{
            $query1="SELECT * FROM  $forward where nota IN (SELECT nota FROM transaksimasuk) and tglbayar like '$tahun-$bulan-%' order by no ";
          }
          $hasil = mysqli_query($conn,$query1);
          $no = 1;
          while ($fill = mysqli_fetch_assoc($hasil)){
        ?>
            <tbody>
            <tr>
            <td><?php echo ++$no_urut;?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['nota']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['tglbayar']); ?></td>
        <?php
            $nota = $fill['nota'];
            $sqle="SELECT COUNT( nota ) AS data FROM transaksimasuk WHERE nota ='$nota'";
            $hasile=mysqli_query($conn,$sqle);
            $rowa=mysqli_fetch_assoc($hasile);
            $jumlahbayar=$rowa['data'];
        ?>
            <td><?php  echo mysqli_real_escape_string($conn, $jumlahbayar); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['total']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['keluar']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['total']-$fill['keluar']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['kasir']); ?></td>
            <td>
            </tr>
        <?php
          }
        ?>
          </tbody></table>
        <?php } ?>

        <?php if($forward == 'supplier'){
          
          $forward = 'supplier';
          
        ?>
          
          <table class="table">
          <thead>
          <tr>
          <th>No</th>
          <th>Kode</th>
          <th>Tgldaftar</th>
          <th>Nama</th>
          <th>PIC</th>
          <th>PKP</th>
          <th>Alamat</th>
          <th>Kota</th>
          <th>Keterangan</th>
          <th>Nohp</th>
          </tr>
          </thead>
        <?php
          error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
          $query1="SELECT * FROM  $forward order by no ";
          $hasil = mysqli_query($conn,$query1);
          $no = 1;
          while ($fill = mysqli_fetch_assoc($hasil)){
        ?>
            <tbody>
            <tr>
            <td><?php echo ++$no_urut;?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['kode']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['tgldaftar']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['pic']); ?></td>
            <?php
                
                if($fill['pkp'] == 1){
                  $pkp = "PKP";
                }else{
                  $pkp = "Non PKP";
                }
            ?>
            <td><?php  echo $pkp; ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['alamat']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['kota']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['keterangan']); ?></td>
            <td><?php  echo mysqli_real_escape_string($conn, $fill['nohp']); ?></td>
            </tr>
        <?php
          }
        ?>
          </tbody></table>
        <?php } ?>
          
          
          <?php if($forward == 'operasional'){ ?>
            
            <table class="table">
            <thead>
            <tr>
            <th>No</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Tanggal</th>
            <th>Biaya</th>
            <th>Keterangan</th>
            <th>Cc</th>
            </tr>
            </thead>
            <?php
            error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
            if($tahun == null || $tahun == ""){
              $query1="SELECT * FROM  $forward order by no ";
            }else{
              $query1="SELECT * FROM  $forward where tanggal like '$tahun-$bulan-%' order by no ";
            }
            $hasil = mysqli_query($conn,$query1);
            $no = 1;
            while ($fill = mysqli_fetch_assoc($hasil)){
              ?>
              <tbody>
              <tr>
              <td><?php echo ++$no_urut;?></td>
              <td><?php  echo mysqli_real_escape_string($conn, $fill['kode']); ?></td>
              <td><?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?></td>
              <td><?php  echo mysqli_real_escape_string($conn, $fill['tanggal']); ?></td>
              <td><?php  echo mysqli_real_escape_string($conn, number_format($fill['biaya'])); ?></td>
              <td><?php  echo mysqli_real_escape_string($conn, $fill['keterangan']); ?></td>
              <td><?php  echo mysqli_real_escape_string($conn, $fill['kasir']); ?></td>
              </tr><?php
              ;
            }
            
            ?>
            </tbody></table>
            <?php } ?>
            