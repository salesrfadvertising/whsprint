<!DOCTYPE html>
<html>
<?php
// include "configuration/config_etc.php";
// include "configuration/config_include.php";
function connect() { include 'configuration/config_connect.php'; } // Connect Configuration
function body() { include 'component/core/body.php'; } // Body Component
function footer() { include 'component/core/footer.php'; } // Footer Component
function head() { include 'component/core/head.php'; } // Head Component
function theader() { include 'component/core/theader_home.php'; } // Header Component
function timing() { include 'configuration/config_time.php'; } // Timing Two Configuration
function pagination() { include 'configuration/config_pagination.php'; } // Pagination Configuration
include "configuration/config_alltotal.php";
connect();head();body();timing();
//alltotal();
pagination();
?>
<div class="wrapper">
<?php
theader();
?>
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
<?php
$decimal ="0";
$a_decimal =",";
$thousand =".";
?>
<div class="content-wrapper">
<section class="content-header">
</section>
<section class="content">
<div class="row">
<div class="col-lg-3 col-xs-6">
<!-- small box -->
<div class="small-box bg-aqua">
<div class="inner">
<h3><sup style="font-size: 20px"></sup><?php echo number_format($stok1, $decimal, $a_decimal, $thousand).' '; ?>Pcs</h3>
<p>Stok Tersedia</p>
</div>
<div class="icon">
<i class="ion ion-stats-bars"></i>
</div>

</div>
</div>
<!-- ./col -->
<div class="col-lg-3 col-xs-6">
<!-- small box -->
<div class="small-box bg-yellow">
<div class="inner">
<h3><?php echo $stok2; ?></h3>
<p>Barang keluar</p>
</div>
<div class="icon">
<i class="ion ion-stats-bars"></i>
</div>

</div>
</div>
<!-- ./col -->
<div class="col-lg-3 col-xs-6">
<!-- small box -->
<div class="small-box bg-green">
<div class="inner">
<h3><?php echo $stok3; ?></h3>
<p>Barang Masuk</p>
</div>
<div class="icon">
<i class="ion ion-stats-bars"></i>
</div>

</div>
</div>
<!-- ./col -->
<div class="col-lg-3 col-xs-6">
<!-- small box -->
<div class="small-box bg-red">
<div class="inner">
<h3><?php echo $stok4; ?></h3>
<p>Jumlah Produk</p>
</div>
<div class="icon">
<i class="ion ion-stats-bars"></i>
</div>

</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">

<!-- SETTING START-->

<?php
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
include "configuration/config_chmod.php";
$halaman = "home_a"; // halaman
$dataapa = "Stok Barang"; // data
$tabeldatabase = "barang"; // tabel database
$chmod = $chmenu8; // Hak akses Menu
$forward = mysqli_real_escape_string($conn, $tabeldatabase); // tabel database
$forwardpage = mysqli_real_escape_string($conn, $halaman); // halaman
$search = $_POST['search'];

?>

<!-- SETTING STOP -->

<textarea id="printing-css" style="display:none;">.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
<script type="text/javascript">
function printDiv(elementId) {
  var a = document.getElementById('printing-css').value;
  var b = document.getElementById(elementId).innerHTML;
  window.frames["print_frame"].document.title = document.title;
  window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
  window.frames["print_frame"].window.focus();
  window.frames["print_frame"].window.print();
}
</script>

<!-- BREADCRUMB -->

<ol class="breadcrumb ">
<li><a href="<?php echo $_SESSION['baseurl']; ?>">Home Imarah System </a></li>
<?php

if ($search != null || $search != "") {
  ?>
  <li> <a href="<?php echo $halaman;?>">Data <?php echo $dataapa ?></a></li>
  <li class="active"><?php
  echo $search;
  ?></li>
  <?php
} else {
  ?>
  <li class="active">Data <?php echo $dataapa ?></li>
  <?php
}
?>
</ol>

<!-- BREADCRUMB -->

<!-- BOX HAPUS BERHASIL -->

<script>
window.setTimeout(function() {
  $("#myAlert").fadeTo(500, 0).slideUp(1000, function(){
    $(this).remove();
  });
}, 5000);
</script>

<?php
$hapusberhasil = $_POST['hapusberhasil'];

if ($hapusberhasil == 1) {
  ?>
  <div id="myAlert"  class="alert alert-success alert-dismissible fade in" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Berhasil!</strong> <?php echo $dataapa;?> telah berhasil dihapus dari Data <?php echo $dataapa;?>.
  </div>
  
  <!-- BOX HAPUS BERHASIL -->
  <?php
} elseif ($hapusberhasil == 2) {
  ?>
  <div id="myAlert" class="alert alert-danger alert-dismissible fade in" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Gagal!</strong> <?php echo $dataapa;?> tidak bisa dihapus dari Data <?php echo $dataapa;?> karena telah melakukan transaksi sebelumnya, gunakan menu update untuk merubah informasi <?php echo $dataapa;?> .
  </div>
  <?php
} elseif ($hapusberhasil == 3) {
  ?>
  <div id="myAlert" class="alert alert-danger alert-dismissible fade in" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Gagal!</strong> Hanya user tertentu yang dapat mengupdate Data <?php echo $dataapa;?> .
  </div>
  <?php
}

?>


<?php
  $sqla="SELECT no, COUNT( * ) AS totaldata FROM $forward";
  $hasila=mysqli_query($conn,$sqla);
  $rowa=mysqli_fetch_assoc($hasila);
  $totaldata=$rowa['totaldata'];
?>
  <div class="box" id="tabel1">
    <div class="box-header">
      <h3 class="box-title">Data <?php echo $dataapa ?>  <span class="no-print label label-default" id="no-print"><?php echo $totaldata; ?></span>
      <div class="no-print" id="no-print">
        <div class="box-body">
          <div class="col-md-3">
            <form method="post">
              <div class="input-group input-group-sm" style="width: 250px;">
                <input type="text" name="search" class="form-control pull-right" placeholder="Cari">
                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
          
          <div class="col-md-6">
            <form method="get" action="">
              <div class="col-md-3">
                <div class="input-group input-group-sm">
                    <select class="form-control select2" name="kategori" id="kategori">
                      <option selected="selected" value="">Pilih Kategori</option>
                        <?php
                        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                        $sql=mysqli_query($conn,"select *,kategori.nama as nama, kategori.keterangan as keterangan from kategori");
                        while ($row=mysqli_fetch_assoc($sql)){
                          echo "<option value='".$row['keterangan']."'>".$row['nama']."</option>";
                        }
                        ?>
                    </select>
                </div>
              </div>
              <div class="col-md-2">
                <div class="input-group-btn">
                  <button type="submit" name="find" class="btn bg-maroon">Tampilkan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        

      </div>
    </div>
  <!-- /.box-header -->
  <!-- /.Paginasi -->
  <?php
  if(isset($_GET["find"])){
    if($_GET['kategori']!=""){
      $kategori = $_GET['kategori'];
      $sql    = "select * from barang where kategori LIKE '$kategori%' order by nama asc, kategori asc";
      $result = mysqli_query($conn, $sql);
    }else{
      $sql    = "select * from barang order by nama asc, kategori asc";
      $result = mysqli_query($conn, $sql);
    }
  }else{
    $sql    = "select * from barang order by nama asc, kategori asc";
    $result = mysqli_query($conn, $sql);
  }
  error_reporting(E_ALL ^ E_DEPRECATED);
  $rpp    = 250;
  $reload = "$halaman"."?pagination=true";
  $page   = intval(isset($_GET["page"]) ? $_GET["page"] : 0);
  
  if ($page <= 0)
  $page = 1;
  $tcount  = mysqli_num_rows($result);
  $tpages  = ($tcount) ? ceil($tcount / $rpp) : 1;
  $count   = 0;
  $i       = ($page - 1) * $rpp;
  $no_urut = ($page - 1) * $rpp;
  ?>
  <div class="box-body table-responsive">
  <table  class="table table-bordered table-hover" id="example2" style="overflow:auto" cellspacing="0">
  <thead>
  <tr>
  <th>No</th>
  <th>Kode </th>
  <th>Nama </th>
  <th>Merek</th>
  <th>Kategori</th>
  <!-- <th>Stok Keluar</th>
  <th>Stok Masuk</th> Edited by syafaat-->
  <th>Stok Aktual</th>
  <th>Satuan</th>
  </tr>
  </thead>
  <?php
  error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
  $search = $_POST['search'];
  
  if ($search != null || $search != "") {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if(isset($_POST['search'])){
        $query1="select * from barang where kode like '%$search%' or nama like '%$search%' or brand like '%$search%' or kategori like '%$search' order by barang.no limit $rpp";
        $hasil = mysqli_query($conn,$query1);
        $no = 1;
        while ($fill = mysqli_fetch_assoc($hasil)){
          ?>
          <tbody>
          <tr>
          <td><?php echo ++$no_urut;?></td>
          <td><?php  echo mysqli_real_escape_string($conn, $fill['sku']); ?></td>
          <td><?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?></td>
          <td><?php  echo mysqli_real_escape_string($conn, $fill['brand']); ?></td>
          <td><?php  echo mysqli_real_escape_string($conn, $fill['kategori']); ?></td>
          <!-- <td><?php  //echo mysqli_real_escape_string($conn, $fill['terjual']); ?></td>
          <td><?php  //echo mysqli_real_escape_string($conn, $fill['terbeli']); ?></td> Edited by syafaat-->
          <td><?php  echo mysqli_real_escape_string($conn, $fill['sisa']); ?></td>
          <td><?php  echo mysqli_real_escape_string($conn, $fill['satuan']); ?></td>
          </tr>
          <?php ;
        } ?>
        </tbody></table>
        <div align="right"><?php if($tcount>=$rpp){ echo paginate_one($reload, $page, $tpages);}else{} ?></div>
        <?php
      }
      
    }
    
  } else {
    while(($count<$rpp) && ($i<$tcount)) {
      mysqli_data_seek($result,$i);
      $fill = mysqli_fetch_array($result);
      ?>
      <tbody>
      <tr>
      <td><?php echo ++$no_urut;?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['sku']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['nama']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['brand']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['kategori']); ?></td>
      <!-- <td><?php  //echo mysqli_real_escape_string($conn, $fill['terjual']); ?></td>
      <td><?php  //echo mysqli_real_escape_string($conn, $fill['terbeli']); ?></td> -->
      <td><?php  echo mysqli_real_escape_string($conn, $fill['sisa']); ?></td>
      <td><?php  echo mysqli_real_escape_string($conn, $fill['satuan']); ?></td>
      </tr>
      <?php
      $i++;
      $count++;
    }
    
    ?>
    </tbody></table>
    <div align="right"><?php if($tcount>=$rpp){ echo paginate_one($reload, $page, $tpages);}else{} ?></div>
    <?php } ?>
    
    </div>
    <!-- /.box-body -->
    </div>
    <div align="right"  style="padding-right:15px"  class="no-print" id="no-print" >
    <div align="left" class="no-print" id="no-print"> <a onclick="javascript:printDiv('tabel1');" class="btn btn-default btn-flat" name="cetak" value="cetak"><span class="glyphicon glyphicon-print"></span></a><?php echo " "; ?>
    <a onclick="window.location.href='configuration/config_export?forward=<?php echo $forward; ?>&search=<?php echo $search; ?>'" class="btn btn-default btn-flat" name="cetak" value="export excel"><span class="glyphicon glyphicon-save-file"></span></a></div> <br/>
    </div>
    </div>
    <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
    </div>
    <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php footer();?>
    <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="dist/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="dist/plugins/morris/morris.min.js"></script>
    <script src="dist/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="dist/plugins/knob/jquery.knob.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="dist/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="dist/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="dist/plugins/fastclick/fastclick.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/pages/dashboard.js"></script>
    <script src="dist/js/demo.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>
    <script>
          $(document).ready(function () {
            var table = $('#example2').DataTable({
              dom: 'Bfrtip',
              buttons: [
                  'excel','print'
              ],
              aLengthMenu: [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
              ],
              iDisplayLength: -1,
              "paging": true,
              "pageLength": 50,
              "lengthChange": true,
              "searching": false,
              "ordering": true,
              "info": true,
            });
          });
        </script>
    </body>
    </html>
    